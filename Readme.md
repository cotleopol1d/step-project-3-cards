
## Project participants:
- Viktoriia Grankina;
- Vitaliy Talko;
- Olexandr Zayats.

## Task distribution:
- HTML & CSS, testing  - team work;
- Viktoriia Grankina  - input.js, login.js, modal.js, sortable.js;
- Vitaliy Talko - requests.js, visit.js;
- Olexandr Zayats - filter.js.
